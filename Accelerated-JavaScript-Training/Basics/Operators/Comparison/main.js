//console.log(NaN == NaN);
//false

// console.log(0 == null);
// false --> You can't compare null

//console.log(null == undefined); 
//true --> exception

//console.log(null == undefined);
//false

// Null can't be compared, except to undefined. And undefined compared to anything will return false, except to null.