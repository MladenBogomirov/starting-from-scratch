var http = new XMLHttpRequest();

http.open('GET', 'https://jsonplaceholder.typicode.com/posts');
http.onreadystatechange = function () {
  if (
    http.readyState === XMLHttpRequest.DONE &&
    http.status === 200) {
      console.log(JSON.parse(http.responseText));
    } else if (
      http.readyState === XMLHttpRequest.DONE &&
      http.status !== 200) {
        console.log('Error!');
      }
}

http.send();