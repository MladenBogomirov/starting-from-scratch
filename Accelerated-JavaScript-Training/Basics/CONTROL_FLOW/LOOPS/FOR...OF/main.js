//Iterating over an Array
// const array = [1,2,3,4,5];

// for (let i of array) {
//   console.log(i);
// }

//Iterating over a String

const string = 'Hello World!';
for (let i of string) {
  if (i === 'W') {break}
  console.log(i);
}