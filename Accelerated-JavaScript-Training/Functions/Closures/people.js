const peopleList = (function() {
  const people = [];

  const addPerson = (name, age, gender) => {
    if (typeof name !== 'string') {
      throw 'Name is not a string';
    }

    if (!name.match(/^[A-Z][a-z]*$/)) {
      throw 'Name contains invalid symbols!';
    }

    people.push({
      name: name,
      gender: gender,
      age: age
    });    
  };

  const getPeople = () => {
    return people.map(p => {
      return {
        name: p.name,
        age: p.age,
        gender: p.gender  
      }
    });
  };

  return {
    addPerson: addPerson, 
    getPeople: getPeople
  };
})();


peopleList.addPerson('Cuki', 20, 'male');

let plist = peopleList.getPeople();

plist[0].name = 'Baba mi';

console.log(plist);

plist = peopleList.getPeople();

console.log(plist);

peopleList.addPerson('Pesho', 50, 'male');
peopleList.addPerson('Snezhi', 25, 'female');
plist = peopleList.getPeople();

console.log(plist);