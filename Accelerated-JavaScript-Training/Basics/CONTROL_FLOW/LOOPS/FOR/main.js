// for (var i = 0; i < 5; i++) {
//   console.log(i);
// }


// NESTED LOOPS
// for (var i = 0; i < 5; i++) {
//   for (var j = 0; j < 2; j++) {
//     console.log(i, j);
//   }
// }

// DESCENDING LOOP
// for (var i = 5; i >= -5; i--) {
//   console.log(`The value of i is ${i}`); 
// }

//What will be printed?
/*for (let i = 0; i < 5; i++) {
  setTimeout(function() {
    console.log(i);
  }, 2000);
}*/

//LOOPING THROUGH ARRAYS
/*var array = [1, 2, 3];

for (var i = 0; i < array.length; i++) {
  console.log(array[i]);
}*/

/*var array = [1, 2, 3, 4, 5, 6, 7];

for (var i = array.length; i > 0; i--) {
  console.log(i);
}*/