var array = [1,2,3,4];

if (array.indexOf('new') === -1) {
  array.unshift('new');
}

console.log(array);

// var newArray = array.splice(2, 2);

// console.log(newArray);
// console.log(array);
// splice() --> mutates the array


var newArray = array.slice(2, 4);

console.log(newArray);
console.log(array);
// slice() --> does not mutate the array