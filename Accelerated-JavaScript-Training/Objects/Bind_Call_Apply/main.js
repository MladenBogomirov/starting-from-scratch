//General rule of thumb: this typically refers to the part, left of the dot (the 'caller' of the function).

function fn(message) {
  console.log(message + this);
}

var obj = {
  objFn: fn
};

var person = {
  name: 'Max'
};

//bind() doesn't call the function automatically. Can be used later, because it's not called instanstly.
//obj.objFn.bind(person, 'Hello from bind')();


//call() calls the function automatically. Parenthesis must be ommitted/calls the method on its own.
//obj.objFn.call(person, 'Hello from call');

//apply() is the same as call(), but the arguments are passed as an array. Parenthesis must be ommitted/calls the method on its own.
//obj.objFn.apply(person, ['Hello from apply']);