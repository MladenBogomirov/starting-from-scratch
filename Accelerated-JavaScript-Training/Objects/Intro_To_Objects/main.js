var person = {
  name: 'Max',
  age: 27,
};

var anotherPerson = new Object();
anotherPerson.name = 'Anna';
anotherPerson.age = 30;

var anotherPerson2 = Object.create(person);
anotherPerson2.name = 'Michael';

console.log(anotherPerson2.age);
