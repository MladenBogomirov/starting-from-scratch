// Given an array of digitals numbers, 
//return a new array of length "number"
//containing the last even numbers from the original array (in the same order). 
//The original array will be not empty and will contain at least "number" even numbers.

function evenNumbers(array, number) {
  let evenNumbersArray = [];
  let reversedArray = array.reverse();

  for (let i = 0; i < reversedArray.length; i++) {
    if (evenNumbersArray.length !== number) {
      if (array[i] % 2 === 0) {
        evenNumbersArray.unshift(array[i]);
      }
    }
  }
  return evenNumbersArray;
}


// Another possible solution:
// const isEven = number => n % 2 === 0;
// const evenNumbers = (array, number) => a.filter(isEven).slice(-n);