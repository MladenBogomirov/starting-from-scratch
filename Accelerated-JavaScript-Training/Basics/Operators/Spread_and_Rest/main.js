//The Spread operator expands an array, object, string or any other iterable.
// const fruits = ['Grapes', 'Apple', 'Strawberry'];
// console.log(fruits);

// const newFruits = [...fruits, 'Mango', 'Cherry'];

// console.log(newFruits);

//The rest operator is used to combine multiple elements and is particularly useful during array and object destructuring.

// let people = ['Steve', 'Pete', 'Mike', 'Nick'];

// let [doctor, lawyer, ...students] = people;

// console.log(doctor, lawyer, students);

const {name, age, ...otherDetails} = {
  name: 'Steve',
  age: 19,
  place: 'Romania',
  hobby: 'reading'
}

console.log(name);

