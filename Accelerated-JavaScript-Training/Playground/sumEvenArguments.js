//Write a function called sumEvenArguments which takes all of the arguments passed to a function and returns the sum of the even ones

// function sumEvenArguments() {
//   let sum = 0;
//   const arr = [].slice.call(arguments);

//   arr.reduce((_acc, currValue) => {
//     if (currValue % 2 === 0) {
//       sum += currValue;
//     }
//   }, 0);
//   return sum;
// }

function sumEvenArguments(...numbers) {
  let sum = 0;
  numbers.reduce((acc, currentValue) => {
    if (currentValue % 2 === 0) {
      sum += currentValue;
    }
  }, 0);
  return sum;
}

console.log(sumEvenArguments(320, 123, 180, 222, 489));