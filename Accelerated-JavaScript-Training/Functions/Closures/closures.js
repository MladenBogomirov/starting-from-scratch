// //Function defined in another function. Very useful for callbacks.
// var otherNum = 30;
// function generator(input) {
//   var number = input + otherNum;
//   return function() { //this function is a closure and it's aware of its environment
//     return number * 2;
//   };
// }

// var calc = generator(10);
// console.log(calc());

// let createCounter = () => {
//   let counter = 0;

//   return addToCounter = () => {
//     counter += 1; 
//     return counter;
//   }
// }

// let newCounter = createCounter();
// console.log(newCounter());
// console.log(newCounter());
// console.log(newCounter());
// console.log(newCounter());