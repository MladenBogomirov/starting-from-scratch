const person = {
  name: 'Max',
  age: 29,
  greet() {
    console.log('Hi, I am ' + this.name);
  }
};

const hobbies = ['Sports', 'Cooking'];

//const copiedArray = hobbies.slice();

//Sread operator
const copiedArray = [...hobbies];
console.log(copiedArray);

//Rest operator - merges all arguments in an array
const toArray = (...args) => {
  return args
}

console.log(toArray('babami','dedami','chichami'));
