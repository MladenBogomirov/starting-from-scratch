var array = [1,2,3,4];

if (array.indexOf('new') === -1) {
  array.unshift('new');
}

// console.log(array.filter(function (value) {
//   return value > 2;
// }));

// console.log(array.map(function (val) { //Does not mutate
//  return val * 2;
// }));

console.log(array.reverse()); //Mutates the original array
console.log(array)