// firstDuplicate = a => {
//   r = new Set()
//   for (e of a)
//       if (r.has(e))
//           return e
//       else
//           r.add(e)
//   return -1
// }



// If a number is found first time make a[abs(number) - 1] negative
// If a[abs(number) - 1] is negative it means number is found for second time
// return it. 
function firstDuplicate(a) {
  for (var i = 0; i < a.length; i++) {
      var num = a[i];
      if ( a[Math.abs(num) - 1] > 0 ) {
          a[Math.abs(num) - 1] = -1 * a[Math.abs(num) - 1];
      }
      else {
          return Math.abs(num);
      }
  }
  
  return -1;
}