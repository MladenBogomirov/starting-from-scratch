function sumDigits(number) {
  const stringifiedNumber = Math.abs(number).toString().split('');
  let result = 0;
  for (i of stringifiedNumber) {
    result += parseInt(i)
  }
  return result;
}

// function sumDigits(number) {
//   const stringifiedNumber = Math.abs(number).toString().split('');

//   return stringifiedNumber.reduce((accumulator, currentValue) => {
//     return +accumulator + +currentValue; //Use the + signs to convert string to number
//   });
// }

console.log(sumDigits(-12));