//General rule of thumb: this typically refers to the part, left of the dot (the 'caller' of the function).

function fn() {
  console.log(this);
}

var obj = {
  objFn: fn
};

var person = {
  name: 'Max'
};

//obj.objFn.bind(this)();
//rebind objFn to the global

obj.objFn.bind(person)();
//rebind objFn to the person object