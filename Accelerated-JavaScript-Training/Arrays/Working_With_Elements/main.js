var array = [1,2,3,4];

// Adds an item at the end of the array
// array.push(4)
// console.log(array);



// Removes and returns an item from the end of the array
// console.log(array.pop());
// console.log(array);



// Removes and returns an item at the beginning of the array
// console.log(array.shift());
// console.log(array);



// Adds an item at the beginning of the array
// array.unshift('New item');
// console.log(array);