const innerDiv = document.querySelector('#inner');

const outerDiv = document.querySelector('#outer');

function innerListener(event) {
  console.log('Clicked inner');
}

function outerListener(event) {
  console.log('Clicked outer');
}

innerDiv.addEventListener('click', innerListener);
outerDiv.addEventListener('click', outerListener);