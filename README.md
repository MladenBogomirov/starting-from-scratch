# Starting from scratch

So, I decided to go back into learning web development. I will start from scratch with basic stuff like beginner HTML, CSS and JS, and move on to more advanced topics like SASS, React, Vue etc.