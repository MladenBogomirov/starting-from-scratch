//VARIABLES: DECLARING AND HOISTING

'use strict';
// x=3
// var x;
// let y;
// y='asd';
// const z;

// console.log(x, y, z);

// SyntaxError: Missing initializer in const declaration. => const declarations has to be initialized on declaration
// Only var declarations get hoisted. let and const - don't!

var shape = 'square';
console.log(shape);