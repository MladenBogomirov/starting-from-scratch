const p = document.createElement('P');
p.textContent = 'A new paragraph';
p.style.fontSize = '17px';

const li = document.querySelector('li');
const a = li.firstElementChild;

li.appendChild(p); //Appends at the end of all the other childs

li.insertBefore(p, a); //Appends at the end of all the other childs