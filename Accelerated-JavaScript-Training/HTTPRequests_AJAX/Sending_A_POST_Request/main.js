var http = new XMLHttpRequest();

var data = 'title=Post%20Title&body=Body';

http.open('POST', 'https://jsonplaceholder.typicode.com/posts');
http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
http.onreadystatechange = function () {
  if (
    http.readyState === XMLHttpRequest.DONE &&
    http.status === 201) {
      console.log(JSON.parse(http.responseText));
    } else if (
      http.readyState === XMLHttpRequest.DONE &&
      http.status !== 201) {
        console.log('Error!');
      }
}

http.send(data);