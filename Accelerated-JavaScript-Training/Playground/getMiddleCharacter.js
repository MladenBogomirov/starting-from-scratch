const getMiddle = (s) => {
  const midChar = s[Math.round(s.length / 2) - 1];
  if (s.length % 2 !== 0) {
    return midChar;
  } 
  return midChar + s[Math.round(s.length / 2)];
}