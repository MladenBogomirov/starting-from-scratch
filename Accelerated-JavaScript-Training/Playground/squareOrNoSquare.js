function squareOrSquareRoot(array) {
    const resultArray = [];
    array.map((integer) => {
    if (Math.sqrt(integer) % 1 === 0) {
      resultArray.push(Math.sqrt(integer))
    } else {
      resultArray.push(integer * integer);
    }
  });

  return resultArray;
}

console.log(squareOrSquareRoot([4,3,9,7,2,1]));
