const BTN = document.querySelector('button');

function listener1() {
  console.log('listener 1');
}

function listener2() {
  console.log('listener 2');
}

BTN.addEventListener('click', listener1);
// BTN.addEventListener('click', listener2);

// setTimeout(() => {
//   BTN.removeEventListener('click', listener1);
// }, 2000);

