const account = {
  cash: 12000,
  _name: 'Default',
  withdraw: function(amount) {
    this.cash -= amount;
    console.log(`Withdrew ${amount}, new cash reserve is: ${this.cash}`);
  }
}

//defineProperty() defines only read-only properties. We have to explicitly set the writeable property to true.

Object.defineProperty(account, 'deposit', {
  value: function(amount) {
    this.cash += amount;
  }
});

Object.defineProperty(account, 'name', {
  get: function() {
    return this._name;
  },
  set: function(name) {
    if (name === 'Max') {
      this._name = name;
    }
  }
});

console.log(account.name);

account.name = 'ID000-3';

console.log(account.name);
