function Person(name, age) {
  this.name = name;
  this.age = age;
}

var person = new Person('Max');

console.log(person);
