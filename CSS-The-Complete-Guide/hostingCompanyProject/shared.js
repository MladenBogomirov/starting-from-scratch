const backdrop = document.querySelector('.backdrop');
const modal = document.querySelector('.modal');
const selectPlanButtons = document.querySelectorAll('.plan button');
const closeModalButton = document.querySelector('.modal__action--negative');
const toggleButton = document.querySelector('.toggle-button');
const mobileNav = document.querySelector('.mobile-nav');
const ctaButton = document.querySelector('.main-nav__item--cta');
const plans = document.querySelectorAll('.plan');


//console.dir(selectPlanButtons);

//backdrop.style.display = 'block';

for (let i = 0; i < selectPlanButtons.length; i++) {
  selectPlanButtons[i].addEventListener('click', () => {
    // modal.style.display = 'block';
    // backdrop.style.display = 'block';
    modal.classList.add('open');
    backdrop.style.display = 'block';
    setTimeout( () => {
      backdrop.classList.add('open');
    }, 10);
  })
}

const closeModal = () => {
  // modal.style.display = 'none';
  // backdrop.style.display = 'none';
  if (modal) {
    modal.classList.remove('open');
  }
  backdrop.style.display = 'none';
  setTimeout( () => {
    backdrop.classList.remove('open');
  }, 10);
}

closeModalButton.addEventListener('click', closeModal);

toggleButton.addEventListener('click', () => {
  mobileNav.classList.add('open');
  backdrop.style.display = 'block';
  setTimeout( () => {
    backdrop.classList.add('open');
  }, 10);
});

backdrop.addEventListener('click', () => {
  // mobileNav.style.display = 'none';
   mobileNav.classList.remove('open');
   closeModal();
 });


 ctaButton.addEventListener('animationstart', () => {
  console.log('Animation started', event);
 });

 ctaButton.addEventListener('animationend', () => {
  console.log('Animation ended', event);
 });

 ctaButton.addEventListener('animationiteration', () => {
  console.log('Animation iteration', event);
 });

 plans.forEach( (plan)=> {
  plan.addEventListener('mouseover', () => {
    plan.style.animation = 'push-up .2s ease-out forwards';
  });
  plan.addEventListener('mouseout', () => {
    plan.style.animation = 'push-down .2s ease-out forwards';
  });
 })