const name = 'Max';
let age = 29;
const hasHobbies = true;

age = 30;

function summarizeUser(userName, userAge, userHobby) {
  return (
    'Name is ' + 
    userName + 
    ', age is ' + 
    userAge + 
    ' and the user has hobbies: ' +
    userHobby
  );
}

console.log(summarizeUser(name, age, hasHobbies));