/******USEFUL RESOURCES******/

//https://javascriptweblog.wordpress.com/2010/07/06/function-declarations-vs-function-expressions/





/*THIS IS A FUNCTION DECLARATION. IT IS HOISTED*/ 
// function calc() {
//   console.log('Inside function calc');
// }
// calc();
///////////////////////////////////////


/*THIS IS A FUNCTION EXPRESSION. IT IS NOT HOISTED LIKE DECLARATIONS*/ 
// var calc2 = function () {
//   console.log('Inside function calc2');
// };
// calc2();
///////////////////////////////////////



//*********FUNCTION HOISTING*********//
//Examples 1:

/*function foo(){
  //1. define bar once
 function bar() {
   return 3;
 }
  //3. retutn the value 8
 return bar();
  //2. redefine it
 function bar() {
   return 8;
 }
}
console.log(foo()); //Prints 8*/


//Examples 2:

/*function foo(){
  // 1. A declaration for each function expression
  //      var bar = udnefined
  //      var bar = udnefined

  //2. First Function Expression is executed
  var bar = function() {
      return 3;
  };

  //3. Function created by first Function Expression is invoked
  return bar();

  //4. Second Function Expression unreachable
  var bar = function() {
      return 8;
  };
}
console.log(foo());*/






/**********ARGUEMENTS AND RETURNING VALUES**********/

// function calc () {
//   return 'Inside function!';
// }

// var returned = calc();
// console.log(returned); //Prints what is returned from the function.

// function calc(num1, num2) {
//   return num1 + num2;
// }

// var calculator = calc;
// console.log(calculator(15, 2));