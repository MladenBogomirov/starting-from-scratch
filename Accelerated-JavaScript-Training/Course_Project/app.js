'use strict';

const searchWeather = () => {
  loadingText.style.display = 'block';
  weatherBox.style.display = 'none';
  const cityName = searchCity.value;
  if (cityName.trim().length === 0) {
    return alert('Please enter a city name');
  }
  const http = new XMLHttpRequest();
  const apiKey = '6dc87d7f26188092ec218e7744a1f020';
  const url = `
    https://api.openweathermap.org/data/2.5/weather?q=${cityName}
    &units=metric&appid=${apiKey}
  `;
  const method = 'GET';

  http.open(method, url);
  http.onreadystatechange = function() {
    if (http.readyState ===  XMLHttpRequest.DONE && http.status === 200) {
      const data = JSON.parse(http.responseText);
      const weatherData = new Weather(cityName, data.weather[0].description.toUpperCase());
      weatherData.temperature = `${data.main.temp} C.`;
      updateWeather(weatherData);
      
    } else if (http.readyState === XMLHttpRequest.DONE) {
      alert('Something went wrong! Please, try again!');
    }
  };
  http.send();
}

const updateWeather = (weatherData) => {
  weatherCity.textContent = weatherData.cityName;
  weatherDescription.textContent = weatherData.description;
  weatherTemperature.textContent = weatherData.temperature;
  loadingText.style.display = 'none';
  weatherBox.style.display = 'block';
}

searchButton.addEventListener('click', searchWeather);