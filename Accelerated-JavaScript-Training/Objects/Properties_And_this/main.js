var person = {
  name: 'Max',
  age: 27,
  details: {
    hobbies: ['Sports', 'Cooking'],
    location: 'Germany'
  },
  greet: function(params) {
    console.log(`Hello, I am ${this.name}`);
  }
};

person.greet();