//Coppied by value
// var aNumber = 5;
// console.log(aNumber);


// var anotherNumber = aNumber;
// console.log(anotherNumber);

// aNumber = 12;

// console.log(aNumber);
// console.log(anotherNumber);

//Coppied by reference
// var array = [1, 2, 3];
// var anotherArray = array;

// console.log(array);
// console.log(anotherArray);

// anotherArray = ['error'];

// console.log(array);

//console.log([10] === [10]);
//false


// var oldArray = [];
// var object = {};
// object.newArray = oldArray;
// oldArray.push(10);

// console.log(oldArray);
// console.log(object.newArray);


// console.log(object.newArray === oldArray);
//true

let babami = 'Elena';
let drugataMiBaba = babami;

console.log(babami, drugataMiBaba);

babami = 'Dida';

console.log(babami, drugataMiBaba);
