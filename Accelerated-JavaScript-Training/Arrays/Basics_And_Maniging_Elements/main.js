//Creating an array

// let tasks = [];
// let projects = ['Learn Spanish'];

//let projects2 = [...projects, 'Learn English']; //Will create a new array, and leave the old one (projects) with only 1 item.

/********* push() ***********/

// let projects2 = projects.push('Learn Russian'); //Injects the new item directly to the projects array and leaves projects2 empty.


//projects.push('Learn Malayalam'); // Push() mutates the array
//projects.unshift('Learn French', 'Learn Tamil'); // Unshift() mutates the array

/********* pop() ***********/
// pop() mutates the array
// let tasks = ['Task 1', 'Task 2', 'Task3'];
// let deletedTask = tasks.pop();

// console.log(typeof deletedTask);
// console.log(tasks);


/********* Spread operator ... ***********/

// Adding items with the spread operator. Does not mutate the array and returns a new array.

// projects = ['Learn Malayam', ...projects] ;
// console.log(projects);

// projects = [...projects, 'Learn German', 'Learn Swahili', 'Learn Chinese'];
// console.log(projects);

/********* shift() ***********/

//let numbers = [1,2,3,4];
//let firstNo = numbers.shift(); //shift() removes the first element from an array and returns that removed element. This method changes the length of the array.

//console.log(numbers);
//console.log(firstNo);


/********* slice() ***********/
//let tasks = ['Task1', 'Task2', 'Task3', 'Task4', 'Task5']; //slice() will not modify the initial array

//let slicedTasks = tasks.slice(1, 5);

// console.log(slicedTasks);
// console.log(tasks);

/********* splice() ***********/
//splice() will mutates the array
// let months = ['Jan', 'March', 'April', 'June'];
// months.splice(1, 0, 'Feb');
// months.splice(4, 1, 'May');

// console.log(months);
