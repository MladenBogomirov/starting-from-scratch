let findDigit = function(num, nth){
  //Check if num is negative. If it is, make it positive.
  if (num < 0) {num *= -1;}
  //Check if nth is negative. If it is return -1.
  if (nth <= 0) {return -1};
  let stringifiedNum = num.toString();
  let array = stringifiedNum.split('');
  //Check if nth is larger than num's number of digits.
  if (array.length < nth) {return 0;}
  return parseInt(array[array.length - nth]); 
}
