function squareDigits(num){
  const arrayOfDigits = num.toString().split('');
  let result = arrayOfDigits.map((item) => {
    return item * item;
  });
  return +result.join('');
}

console.log(squareDigits(91));