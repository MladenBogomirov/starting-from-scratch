var array = [1,2,3,4];

if (array.indexOf('new') === -1) {
  array.unshift('new');
}

var newArray = ['join', 'me'];

// console.log(array.concat(newArray)); //Does not mutate the initial arrays
// console.log(array);

console.log(array.join(', ')); //Does not mutate the initial arrays
console.log(array);
console.log(newArray);