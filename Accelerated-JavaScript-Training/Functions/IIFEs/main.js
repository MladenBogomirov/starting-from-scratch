// IIFEs use cases:

// 1. Creating a local scope - if ES6 is not supported

// (function (){
//   var foo = 'bar';
//   console.log(foo);
// })();

// console.log(foo);

// 2. Providing a wrapping scope around a local variable that is accessed by a function returned from the IIFE.

// const uniqueId = ((function () {
//   let count = 0;
//   return function () {
//     ++count;
//     return `id_${count}`;
//   };
// })());

// console.log(uniqueId());
// console.log(uniqueId());
// console.log(uniqueId());
