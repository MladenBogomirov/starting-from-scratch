function LongestWord(sen) { 
  const array = sen.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g,"").split(' ');
  let result = array[0];
  for (let i = 1; i < array.length; i++) {
    if (array[i].length > result.length) {
      result = array[i];    
    }
  }

  return result;
}

console.log(LongestWord('fun&!! time'));