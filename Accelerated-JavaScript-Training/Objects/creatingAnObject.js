// Literal notation - Object.prototype is the prototype
var person = {
  name: 'Max',
  age: 27
}

//new Object()
var person = new Object();
person.name = 'Max';
person.age = 27;

//Object.create() - Does not have Object.prototype
var person = Object.create(null); //Can pick the prototype via argument
person.name = 'Max';
person.age = 27;


// Constructor function
function Person(name, age) {
  this.name = name;
  this.age = age;
}

var person = new Person();