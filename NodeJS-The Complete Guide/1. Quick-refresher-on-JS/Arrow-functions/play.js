const name = 'Max';
let age = 29;
const hasHobbies = true;

age = 30;

const summarizeUser = (userName, userAge, userHobby) => {
  return (
    'Name is ' + 
    userName + 
    ', age is ' + 
    userAge + 
    ' and the user has hobbies: ' +
    userHobby
  );
}

 //const add = (a, b) => a+ b;

//const addOne = a => a + 1;

console.log(summarizeUser(name, age, hasHobbies));