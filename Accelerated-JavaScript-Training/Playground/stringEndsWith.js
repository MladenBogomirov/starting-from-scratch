function solution(str, ending){
  // const regex = new RegExp(ending + '$');
  // return str.match(regex) ? true : false;
  return str.endsWith(ending);
}

console.log(solution('abcde', 'de'));
